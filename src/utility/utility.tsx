export const getLanguageLabel = (lang: string) => {
  switch (lang) {
    case 'en':
      return 'English';
      break;
    case 'ar':
      return 'العربية';
      break;
    default:
      return 'Français';
      break;
  }
};
