export default {
  'tab-home': 'Accueil',
  'tab-details': 'Détails',
  'tab-explore': 'Explorer',
  'tab-setting': 'Paramètres',
  'tab-login': 'Login',
  'set-language': 'Langue',
  'language-setting': 'Configuration de la langue',
  'main-setting': 'Configuration Générale'
};
