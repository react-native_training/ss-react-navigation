export default {
  'tab-home': 'Home',
  'tab-details': 'Details',
  'tab-explore': 'Explore',
  'tab-setting': 'Settings',
  'tab-login': 'Login',
  'set-language': 'Language',
  'language-setting': 'Language Setting',
  'main-setting': 'Main Settings'
};
