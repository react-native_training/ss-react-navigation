export default {
  'tab-home': 'الرئيسية',
  'tab-details': 'التفاصيل',
  'tab-explore': 'البحث',
  'tab-setting': 'الإعدادات',
  'tab-login': 'الدخول',
  'set-language': 'اللغة',
  'language-setting': 'ضبط اللغة',
  'main-setting': 'Main Settings'
};
