import I18n from "react-native-i18n";

import ar from "./locales/ar";
import en from "./locales/en";
import fr from "./locales/fr";

I18n.translations = {
  en,
  fr,
  ar
};

export default I18n;
