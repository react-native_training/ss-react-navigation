import React, { PureComponent } from 'react';
import { getLanguageLabel } from './../utility/utility';
import I18nHandler from './../components/I18nHandler';
import {
  Container,
  Content,
  List,
  ListItem,
  Text,
  Right,
  Left,
  Icon,
  Body
} from 'native-base';

class SettingsScreen extends PureComponent {
  render() {
    const { language } = this.props;
    return (
      <Container>
        <Content>
          <List>
            <ListItem itemHeader first>
              <I18nHandler style={{ textTransform: 'uppercase', fontSize: 14, fontWeight: 'bold', textAlign: 'left'}} i18nKey={'main-setting'}></I18nHandler>
            </ListItem>
            <ListItem
              icon
              onPress={() => this.props.navigation.navigate('Languages')}
            >
              <Left>
                <Icon active name="md-globe" />
              </Left>
              <Body>
                <Text>Language</Text>
              </Body>
              <Right>
                <Text>{getLanguageLabel(language)}</Text>
              </Right>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}

export default SettingsScreen;
