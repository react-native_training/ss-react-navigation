import React from 'react';
import { View, Text, Image } from 'react-native';
import SmartSchoolIcon from "../assets/images/smart_school.png";

class SplashScreen extends React.Component {
  performTimeConsumingTask = async() => {
    return new Promise((resolve) =>
      setTimeout(
        () => { resolve('result') },
        2000
      )
    )
  }

  async componentDidMount() {
    // Preload data from an external API
    // Preload data using AsyncStorage
    const data = await this.performTimeConsumingTask();

    if (data !== null) {
      this.props.navigation.navigate('App');
    }
  }

  render() {
    return (
      <View style={styles.viewStyles}>
        {/* <Text style={styles.textStyles}>
          Smart School
        </Text> */}
        <Image style={ styles.imageStyles } source={ SmartSchoolIcon } />
      </View>
    );
  }
}

const styles = {
  viewStyles: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ffbd4a'
  },
  textStyles: {
    color: 'white',
    fontSize: 40,
    fontWeight: 'bold'
  },
  imageStyles: {
    height: 130,
    width: 130
  }
}

export default SplashScreen;