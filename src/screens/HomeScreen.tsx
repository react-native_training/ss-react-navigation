import React from "react";
import { View, Text } from "react-native";
import { Button } from 'react-native-elements';
import Icon from "react-native-vector-icons/FontAwesome";

import I18nHandler from './../components/I18nHandler';
import HomeSearchBar from '../components/home/HomeSearchBar';
import HomeExploreSchools from '../components/home/HomeExploreSchools';


class HomeScreen extends React.Component {
  render() {
    return (

      <View style={{ flex: 1, flexDirection: 'column', alignItems: "center", 
                  justifyContent: 'center', margin: 10}}>
        <HomeSearchBar />
        <HomeExploreSchools />
        <Button
          icon={
            <Icon
              name="arrow-right"
              size={15}
              color="white"
            />
          }
          title="Go to Details"
          onPress={() => this.props.navigation.navigate('Details')}
        />
      </View>
    );
  }
}

export default HomeScreen;