import React from "react";
import { View, Text } from "react-native";

import I18nHandler from "./../components/I18nHandler";

class DetailsScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <I18nHandler i18nKey={'tab-details'}>This is settings screen</I18nHandler>
      </View>
    );
  }
}

export default DetailsScreen;