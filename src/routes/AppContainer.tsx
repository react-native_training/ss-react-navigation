import {
  createBottomTabNavigator,
  createSwitchNavigator,
  createStackNavigator,
  createAppContainer
} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';
import SplashScreen from '../screens/SplashScreen';
import ExploreScreen from '../screens/ExploreScreen';
import LoginScreen from '../screens/LoginScreen';
import SettingsScreen from '../screens/SettingsScreen';
import LanguageSettingScreen from '../screens/LanguageSettingScreen';
import I18nHandler from '../components/I18nHandler';
import HomeIconWithBadge from '../components/HomeIconWithBadge';
import React from 'react';

const SettingsStack = createStackNavigator(
  {
    Settings: {
      screen: SettingsScreen,
      navigationOptions: {
        header: null,
        title: 'Langues'
      }
    },
    Languages: {
      screen: LanguageSettingScreen,
      navigationOptions: {
        headerTitle: <I18nHandler style={{color: 'black', fontSize: 18, textTransform: 'uppercase'}} i18nKey='language-setting'> Langues </I18nHandler>,      
      }
    }
  }
);

const AppNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-home" size={25} color={tintColor} />
        )
      }
    },
    Details: {
      screen: DetailsScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-expand" size={25} color={tintColor} />
        )
      }
    },
    Explore: {
      screen: ExploreScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-search" size={25} color={tintColor} />
        )
      }
    },
    Settings: {
      screen: SettingsStack,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-settings" size={25} color={tintColor} />
        )
      }
    },
    Login: {
      screen: LoginScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Icon name="md-person" size={25} color={tintColor} />
        )
      }
    }
  },
  {
    initialRouteName: 'Home',
    tabBarOptions: {
      activeTintColor: '#ffbd4a',
      inactiveTintColor: 'gray',
      labelStyle: {
        fontSize: 10,
        textTransform: 'uppercase',
        fontWeight: 'bold'
      },
      style: {
        backgroundColor: 'transparent'
      }
    },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarLabel: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let label = '',
          i18nKey = '',
          style = {
            fontSize: 11,
            color: 'gray',
            textAlign: 'center'
          };
        if (routeName === 'Home') {
          label = 'Accueil';
          i18nKey = 'tab-home';
        } else if (routeName === 'Details') {
          label = 'Détails';
          i18nKey = 'tab-details';
        } else if (routeName === 'Explore') {
          label = 'Explorer';
          i18nKey = 'tab-explore';
        } else if (routeName === 'Settings') {
          label = 'Paramètres';
          i18nKey = 'tab-setting';
        } else if (routeName === 'Login') {
          label = 'Login';
          i18nKey = 'tab-login';
        }
        let result = null;
        if (focused) {
          style.color = '#ffbd4a';
          result = (
            <I18nHandler style={style} i18nKey={i18nKey}>
              {label}
            </I18nHandler>
          );
        } else {
          result = (
            <I18nHandler style={style} i18nKey={i18nKey}>
              {label}
            </I18nHandler>
          );
        }
        return result;
      }
    })
  }
);

const InitialNavigator = createSwitchNavigator({
  Splash: SplashScreen,
  App: AppNavigator
});

export default createAppContainer(InitialNavigator);
