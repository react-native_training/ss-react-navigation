import React from 'react';
import {
  Container,
  Item,
  Icon,
  Input,
  Button,
  Text,
  Header
} from 'native-base';

class HomeSearchBar extends React.Component {
  onChangeSearchText = () => {
    console.log('searching...');
  };

  render() {
    //const { name, badgeCount, color, size } = this.props;
    return (
      <Container>
        <Header searchBar rounded style={{ borderRadius: 5, backgroundColor: 'transparent'}}>
          <Item style={{ backgroundColor: 'white'}}>
            <Icon style={{ color: 'lightgray' }} name="md-search" />
            <Input onChangeText={this.onChangeSearchText}
            placeholder="Search" placeholderTextColor="lightgray"/>
            <Icon style={{ color: 'lightgray' }} name="md-school" />
          </Item>
          <Button>
            <Text>Search</Text>
          </Button>
        </Header>
      </Container>
    );
  }
}

export default HomeSearchBar;
