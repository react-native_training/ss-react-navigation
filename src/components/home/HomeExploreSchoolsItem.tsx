import React from 'react';
import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  Left,
  Thumbnail,
  Body,
  Button
} from 'native-base';
import { Image, Dimensions } from 'react-native';

const deviceWidth = Dimensions.get('window').width;

class HomeExploreSchoolsItem extends React.Component {
  render() {
    return (
      <Card style={{ marginRight: 15}}>
        <CardItem style={{ paddingLeft:0, paddingTop:0, paddingRight:0, paddingBottom:0}}>
          <Body>
            <Image
              source={require('./../../assets/images/logo-ajyal-alhouda.png')}
              style={{ width: deviceWidth / 3, height: deviceWidth / 3, resizeMode: 'cover', flex: 1, backgroundColor: 'bisque' }}
            />
            <Text>{this.props.title}</Text>
          </Body>
        </CardItem>
      </Card>
    );
  }
}

export default HomeExploreSchoolsItem;
