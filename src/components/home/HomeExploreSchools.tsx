import React from 'react';
import {
  Container,
  Content,
  Card,
  CardItem,
  Text,
  Left,
  Thumbnail,
  Body,
  Button
} from 'native-base';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Image, ListView, View } from 'react-native';
import HomeExploreSchoolsItem from './HomeExploreSchoolsItem';
import { FlatList } from 'react-native-gesture-handler';

class HomeExploreSchools extends React.Component {
  data = [
    {
      title: 'something'
    },
    {
      title: 'something'
    },
    {
      title: 'something'
    },
    {
      title: 'something'
    }
  ];

  constructor(props) {
    super(props);
    this.state = {
      data: this.data
    };
  }

  render() {
    return (
      <Container>
        <Content>
          <Text
            style={{
              paddingLeft: 3,
              fontSize: 18,
              marginBottom: 5,
              fontWeight: 'bold',
              color: 'gray'
            }}
          >
            Explore Smart School
          </Text>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={this.state.data}
            renderItem={({ item: rowData, index }) => {
              return <HomeExploreSchoolsItem title={rowData.title + index} />;
            }}
            keyExtractor={(item, index) => index.toString()}
          />
        </Content>
      </Container>
    );
  }
}

export default HomeExploreSchools;
