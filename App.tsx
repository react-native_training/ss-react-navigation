/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/emin93/react-native-template-typescript
 *
 * @format
 */

import React, { Component } from 'react';
import AppContainer from './src/routes/AppContainer';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { View } from 'react-native';
import appReducers from './src/redux/reducers';

export const store = createStore(appReducers);

interface Props {}
export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <View style={{ flex: 1 }}>
          <AppContainer />
        </View>
      </Provider>
    );
  }
}
